class Answer < ActiveRecord::Base
  belongs_to :question
  has_one :result

  validates :answer, presence: true
end
