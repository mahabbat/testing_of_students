class Question < ActiveRecord::Base
  belongs_to :test
  has_many :answers
  accepts_nested_attributes_for :answers, :reject_if => lambda { |a| a[:answer].blank? }, :allow_destroy => true

  validates :question, presence: true
end
