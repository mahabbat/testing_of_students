class Test < ActiveRecord::Base
  has_many :questions
  accepts_nested_attributes_for :questions, :reject_if => lambda { |a| a[:question].blank? }, :allow_destroy => true

  #validates :name, presence: true
  #validates :description, presence: true
end
