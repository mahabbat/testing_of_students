class QuestionsController < ApplicationController

  def index
    @questions = Question.all
  end

  def show
    @question = Question.find(params[:id])
    # @answer = Answer.new
  end

  def new
    @question = Question.new
  end


  def create_question_with_test
    @question = Question.new
    @test = Test.find(params[:id])
  end

  def create
    @question = Question.new(question_params)
    if @question.save
      flash[:notice] = 'Вопрос успешно создан'
      redirect_to @question
    else
      render :action => 'new'
    end
  end

  def edit
    @question = Question.find(params[:id])
  end

  def update
    @question = Question.find(params[:id])
    if @question.update(question_params)
      redirect_to question_url(@question)
    else
      render 'edit'
    end
  end

  def destroy
    @question = Question.find(params[:id])
    @question.destroy
    flash[:success] = 'Вопрос успешно удален'

    redirect_to questions_path
  end

  private

  def question_params
    params.require(:question).permit(:question, :test_id)
  end
end
