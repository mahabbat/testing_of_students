class TestsController < ApplicationController
  def index
    @tests = Test.all
  end

  def show
    @test = Test.find(params[:id])
  end

  # def new
  #   @test = Test.new
  # end

  def new
    @test = Test.new
      question = @test.questions.build
      question.answers.build
  end

  def create
    @test = Test.new(test_params)
    if @test.save
      flash[:notice] = 'Тест успешно создан'
      redirect_to @test
    else
      render :action => 'new'
    end
  end

  def edit
    @test = Test.find(params[:id])
  end

  def update
    @test = Test.find(params[:id])
    if @test.update(test_params)
      redirect_to test_url(@test)
    else
      render 'edit'
    end
  end

  def destroy
    @test = Test.find(params[:id])
    @test.destroy
    flash[:success] = 'Тест успешно удален'

    redirect_to tests_path
  end

  private

  def test_params
    params.require(:test).permit(:name, :description)
  end
end
