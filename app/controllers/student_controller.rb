class StudentController < ApplicationController

  def tests
    @tests = Test.all
  end

  def questions
    test_id = params[:id]
    @test = Test.find test_id
    @questions = Question.where(test_id: @test)
  end


  def answers
    answer_id = params[:id]
    @answer = Answer.find answer_id
  end
end
